from flask import render_template, flash, redirect, url_for, request, abort, jsonify, session
from flask_login import login_required, current_user, LoginManager

from phone_store import app, db, PRODUCT_PER_PAGE
from phone_store.auth.models import Profile, Product, Category, Order, OrderProduct, User
from phone_store.forms import EditProfileForm

login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@app.route("/")
@app.route("/<int:page>", methods=['GET'])
def index(page=1):
    q = request.args.get('q')
    if q:
        product_to_redirect = Product.query.filter_by(name=q).first()
        if not product_to_redirect:
            abort(404)
        return redirect(url_for('show_product', product_id=product_to_redirect.id))
    products = Product.query.paginate(page, PRODUCT_PER_PAGE, True)
    categories = Category.query.all()
    return render_template('index.html', products=products, categories=categories)


@app.route('/autocomplete', methods=['GET'])
def autocomplete():
    search = request.args.get('q')
    query = Product.query.filter(Product.name.ilike('%' + str(search) + '%'))
    results = [product.name for product in query.all()]
    return jsonify(matching_results=results)


@app.route('/category/<int:cat_id>')
def category(cat_id):
    q = request.args.get('q')
    if q:
        product_to_redirect = Product.query.filter_by(name=q).first()
        if not product_to_redirect:
            abort(404)
        return redirect(url_for('show_product', product_id=product_to_redirect.id))
    products = Product.query.all()
    categories = Category.query.all()
    return render_template('category.html', cat_id=cat_id, products=products, categories=categories)


@app.route("/product/<int:product_id>")
def show_product(product_id):
    product = Product.query.filter_by(id=product_id).first()
    if not product:
        abort(404)

    return render_template("product.html", product=product)


@app.route('/shopping-cart', methods=["GET", "POST"])
@login_required
def shopping_cart():
    if request.method == "POST":
        profile = Profile.query.filter_by(user_id=current_user.id).first()
        if not profile:
            flash('Перед оформлением заказа вы должны заполнить свой профиль!', category='danger')
            return redirect(url_for('shopping_cart'))
        surname = profile.surname
        age = profile.age
        mobile_phone = profile.mobile_phone
        delivery_address = profile.delivery_address
        if surname and age and mobile_phone and delivery_address:
            all_products, grand_total, quantity_total = handle_cart()

            order = Order(user_id=current_user.id)

            for product in all_products:
                order_product = OrderProduct(
                    quantity=product['quantity'], product_id=product['id'])
                order.products.append(order_product)

            db.session.add(order)
            db.session.commit()

            session['cart_item'] = {}
            session['all_total_quantity'] = 0
            session['all_total_price'] = 0
            session.modified = True
            return redirect(url_for('orders'))
        else:
            flash('Перед оформлением заказа вы должны заполнить свой профиль!', category='danger')
            return redirect(url_for('shopping_cart'))

    return render_template("shopping-cart.html")


def handle_cart():
    products = []
    grand_total = 0
    item_index = 0
    quantity_total = 0

    for item in session['cart_item']:
        product = Product.query.filter_by(id=session['cart_item'][item]['id']).first()

        total = session['cart_item'][item]['total_price']
        grand_total += total

        quantity_total += session['cart_item'][item]['quantity']

        products.append({'id': product.id, 'name': product.name, 'price':  product.price,
                         'quantity': session['cart_item'][item]['quantity'], 'total': total, 'index': item_index})
        item_index += 1

    return products, grand_total, quantity_total


@app.route('/delete/<string:code>')
@login_required
def delete_product(code):
    try:
        all_total_price = 0
        all_total_quantity = 0
        session.modified = True

        for item in session['cart_item'].items():
            if item[0] == code:
                session['cart_item'].pop(item[0], None)
                if 'cart_item' in session:
                    for key, value in session['cart_item'].items():
                        individual_quantity = int(session['cart_item'][key]['quantity'])
                        individual_price = float(session['cart_item'][key]['total_price'])
                        all_total_quantity += individual_quantity
                        all_total_price += individual_price
                break

        if all_total_quantity == 0:
            session['all_total_quantity'] = 0
            session['all_total_price'] = 0
        else:
            session['all_total_quantity'] = all_total_quantity
            session['all_total_price'] = all_total_price

        return redirect(url_for('shopping_cart'))
    except Exception as e:
        print(e)


@app.route('/add', methods=['POST'])
@login_required
def add_product_to_cart():
    if request.method == 'POST':
        quantity = int(request.form.get('quantity'))
        product = Product.query.filter_by(id=request.form['product_id']).first()

        item = {
            str(product.id): {
                'name': product.name,
                'id': product.id,
                'storage_capacity': product.storage_capacity,
                'quantity': quantity,
                'price': int(product.price),
                'total_price': quantity * product.price
            }
        }

        all_total_price = 0
        all_total_quantity = 0

        session.modified = True
        if 'cart_item' in session:
            if str(product.id) in session['cart_item']:
                for key, value in session['cart_item'].items():
                    if product.id == key:
                        old_quantity = session['cart_item'][key]['quantity']
                        total_quantity = old_quantity + quantity
                        session['cart_item'][key]['quantity'] = total_quantity
                        session['cart_item'][key]['total_price'] = total_quantity * product.price
            else:
                session['cart_item'] = array_merge(session['cart_item'], item)

            for key, value in session['cart_item'].items():
                individual_quantity = int(session['cart_item'][key]['quantity'])
                individual_price = float(session['cart_item'][key]['total_price'])
                all_total_quantity += individual_quantity
                all_total_price += individual_price
        else:
            session['cart_item'] = item
            all_total_quantity += quantity
            all_total_price += quantity * product.price

        session['all_total_quantity'] = all_total_quantity
        session['all_total_price'] = all_total_price

        return redirect(url_for('show_product', product_id=product.id))


def array_merge(first_array, second_array):
    if isinstance(first_array, list) and isinstance(second_array, list):
        return first_array + second_array
    elif isinstance(first_array, dict) and isinstance(second_array, dict):
        return dict(list(first_array.items()) + list(second_array.items()))
    elif isinstance(first_array, set) and isinstance(second_array, set):
        return first_array.union(second_array)
    return False


@app.route("/profile")
@login_required
def profile():
    profile = ""
    try:
        if Profile.query.filter_by(user_id=current_user.id).first().user_id == current_user.id:
            profile = Profile.query.filter_by(user_id=current_user.id).first()
    except AttributeError:
        profile = Profile(user_id=current_user.id)
        db.session.add(profile)
        db.session.commit()
    return render_template("profile.html", profile=profile)


@app.route("/edit_profile", methods=["GET", "POST"])
@login_required
def edit_profile():
    form = EditProfileForm()
    profile = Profile.query.filter_by(user_id=current_user.id).first()
    if form.validate_on_submit():
        profile.surname = form.surname.data
        if not profile.surname.isalpha():
            flash("Фамилия не должна содержать цифры", category='danger')
            return redirect('edit_profile')
        profile.age = form.age.data
        profile.mobile_phone = form.mobile_phone.data
        if not profile.mobile_phone.isnumeric():
            flash("Номер не должен содержать буквы", category='danger')
            return redirect('edit_profile')
        profile.delivery_address = form.delivery_address.data
        db.session.commit()
        flash('Профиль изменен успешно', category="success")
        return redirect(url_for('profile'))
    elif request.method == 'GET':
        form.surname.data = profile.surname
        form.age.data = profile.age
        form.mobile_phone.data = profile.mobile_phone
        form.delivery_address.data = profile.delivery_address
    return render_template('edit_profile.html', form=form, profile=profile)


@app.route('/orders')
@login_required
def orders():
    orders = Order.query.filter_by(user_id=current_user.id).all()
    products = {}
    for order in orders:
        for product in order.products:
            products.update({Product.query.filter_by(id=product.product_id).first(): product.quantity})
    return render_template('orders.html', products=products)