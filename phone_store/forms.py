from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField
from wtforms.validators import InputRequired, Length, NumberRange, Email
from wtforms.fields.html5 import IntegerField, EmailField


class EditProfileForm(FlaskForm):
    surname = StringField('Фамилия', validators=[InputRequired(), Length(min=2, max=30)])
    age = IntegerField("Возраст", validators=[NumberRange(min=10, max=100)])
    mobile_phone = StringField('Номер телефона', validators=[InputRequired(), Length(min=5, max=15)])
    delivery_address = StringField('Адрес доставки', validators=[InputRequired(), Length(min=2, max=100)])

