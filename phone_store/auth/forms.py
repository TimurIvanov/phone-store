from flask_wtf import FlaskForm
from wtforms import PasswordField, BooleanField, StringField
from wtforms.validators import InputRequired, Length, Email, EqualTo
from wtforms.fields.html5 import EmailField


class LoginForm(FlaskForm):
    email = EmailField('email', validators=[InputRequired(), Email(message="Некорректный email"), Length(max=50)])
    password = PasswordField('пароль', validators=[InputRequired(),
                                                   Length(min=6, max=150,
                                                          message="Пароль должен состоять хотя бы из 6 символов")])
    remember = BooleanField('запомнить меня')


class RegisterForm(FlaskForm):
    name = StringField('имя', validators=[InputRequired(), Length(min=2, max=30,
                                                                  message="Имя должно состоять хотя бы из 2 символов")])
    email = EmailField('email', validators=[InputRequired(), Email(message="Некорректный email"), Length(max=50)])
    password = PasswordField('пароль', validators=[InputRequired(),
                                                   Length(min=6, max=150,
                                                          message="Пароль должен состоять хотя бы из 6 символов")])
    confirm_password = PasswordField('повторите пароль',
                                     validators=[InputRequired(),
                                                 EqualTo("password", message="Пароли должны совпадать"),
                                                 Length(min=6, max=150,
                                                        message="Пароль должен состоять хотя бы из 6 символов")])
