from datetime import datetime

from flask_login import UserMixin

from phone_store import db


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=True)
    email = db.Column(db.String(50), unique=True)
    password = db.Column(db.String(150), nullable=True)
    is_admin = db.Column(db.Boolean, default=False)
    date = db.Column(db.DateTime, default=datetime.utcnow)

    profile = db.relationship('Profile', backref='user', uselist=False)
    orders = db.relationship('Order', backref='customer')

    def __repr__(self):
        return f'<User {self.id}>'


class Profile(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    surname = db.Column(db.String(30), nullable=True)
    age = db.Column(db.Integer, nullable=True)
    mobile_phone = db.Column(db.String, nullable=True)
    delivery_address = db.Column(db.String(50), nullable=True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return f'<Profile {self.id}>'


class OrderProduct(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('order.order_id'))
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    quantity = db.Column(db.Integer)


class Order(db.Model):
    order_id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    date = db.Column(db.DateTime, default=datetime.utcnow)

    products = db.relationship('OrderProduct', backref="order", lazy=True)

    def __repr__(self):
        return f'<Order {self.order_id}>'


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=True)
    price = db.Column(db.Integer, nullable=True)
    description = db.Column(db.String(), nullable=True)
    product_img = db.Column(db.String(), default='/images/phone.jpg', nullable=True)
    operating_system = db.Column(db.String(40), nullable=True)
    storage_capacity = db.Column(db.Integer, nullable=True)
    processor = db.Column(db.String(), nullable=True)
    screen_size = db.Column(db.String(10), nullable=True)
    camera_resolution = db.Column(db.String(20), nullable=True)
    weight = db.Column(db.Integer, nullable=True)
    display = db.Column(db.String(50), nullable=True)
    selfie_camera_resolution = db.Column(db.String(20), nullable=True)
    ram = db.Column(db.Integer, nullable=True)
    battery = db.Column(db.Integer, nullable=True)

    category = db.Column(db.Integer, db.ForeignKey('category.id'))
    orders = db.relationship('OrderProduct', backref='product', lazy=True)

    def __repr__(self):
        return f'<Product {self.id}>'


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=True)

    def __repr__(self):
        return f'<Category {self.name}>'
