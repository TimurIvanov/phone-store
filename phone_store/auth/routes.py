from flask import flash, url_for, redirect, request, render_template
from flask_login import LoginManager, login_user, logout_user, login_required
from werkzeug.security import check_password_hash, generate_password_hash

from phone_store.auth.forms import LoginForm, RegisterForm
from phone_store.auth.models import User

from phone_store import db, app
from phone_store.auth import auth

login_manager = LoginManager()
login_manager.login_view = 'auth.login'
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@auth.route('/login', methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)

    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data
        remember = True if form.remember.data else False

        user = User.query.filter_by(email=email).first()

        if not user or not check_password_hash(user.password, password):
            flash('Логин или пароль неправильные', category='danger')
            return redirect(url_for('auth.login'))

        login_user(user, remember=remember)
        flash("Вы успешно авторизовались", category='success')
        return redirect(url_for('index'))

    return render_template('auth/login.html', form=form)


@auth.route('/register', methods=["GET", "POST"])
def register():
    form = RegisterForm(request.form)

    if form.validate_on_submit():
        name = form.name.data
        email = form.email.data
        password = form.password.data

        user = User.query.filter_by(email=email).first()

        if user:
            flash('Пользователь с таким email уже существует', category='danger')
            return redirect(url_for('auth.register'))

        new_user = User(name=name, email=email, password=generate_password_hash(password, method='sha256'))
        db.session.add(new_user)
        db.session.commit()

        login_user(new_user)
        flash("Вы успешно зарегистрированы", category="success")
        return redirect(url_for('index'))

    return render_template('auth/register.html', form=form)


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    flash("Вы вышли со своей учетной записи", category="success")
    return redirect(url_for('index'))
