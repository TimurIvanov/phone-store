import datetime

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


SECRET_KEY = 'lkaehf$@!uabkjf81jl.s[pojhashfb2bs'
MAX_CONTENT_LENGTH = 1024 * 1024
DEBUG = True
SQLALCHEMY_TRACK_MODIFICATIONS = False
PRODUCT_PER_PAGE = 8

app = Flask(__name__, instance_relative_config=True)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://admin:admin@localhost/phone_store'
app.config.from_object(__name__)
app.permanent_session_lifetime = datetime.timedelta(days=7)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from phone_store.auth import auth
from phone_store.admin import admin
app.register_blueprint(auth, url_prefix='/auth')
app.register_blueprint(admin, url_prefix='/admin')

from phone_store import routes

