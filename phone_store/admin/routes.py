import os

from flask_login import login_required, login_user, LoginManager, logout_user
from flask import request, flash, redirect, url_for, render_template
from werkzeug.security import check_password_hash

from phone_store.admin import admin
from phone_store.auth.forms import LoginForm
from phone_store.auth.models import User, Product, Order, OrderProduct
from phone_store import app, db
from phone_store.admin.forms import AddProductForm, ProductEditForm


login_manager = LoginManager()
login_manager.login_view = 'admin.login'
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@admin.route('/login', methods=["GET", "POST"])
def login():
    form = LoginForm(request.form)

    if form.validate_on_submit():
        email = form.email.data
        password = form.password.data
        remember = True if form.remember.data else False

        user = User.query.filter_by(email=email).first()

        if not user or not check_password_hash(user.password, password):
            flash('Логин или пароль неправильные', category='danger')
            return redirect(url_for('admin.login'))

        if not user.is_admin:
            flash('Логин или пароль неправильные', category='danger')
            return redirect(url_for('admin.login'))

        login_user(user, remember=remember)
        flash("Вы успешно авторизовались", category='success')
        return redirect(url_for('admin.admin', slug='products'))

    return render_template('admin/login.html', form=form)


@admin.route('/logout')
@login_required
def logout():
    logout_user()
    flash("Вы вышли со своей учетной записи", category="success")
    return redirect(url_for('admin.login'))


@admin.route('/edit_product/<int:product_id>', methods=["GET", "POST"])
@login_required
def edit_product(product_id):
    form = ProductEditForm()
    product = Product.query.filter_by(id=product_id).first()
    if form.validate_on_submit():
        upload_file = request.files.get('product_img')
        file_path = None
        if upload_file:
            file_name = upload_file.filename
            file_path = os.path.join('images/', file_name)
            upload_file.save(os.path.join('phone_store/static', file_path))
        product.name = form.name.data
        product.price = form.price.data
        product.description = form.description.data
        product.product_img = file_path
        product.operating_system = form.operating_system.data
        product.storage_capacity = form.storage_capacity.data
        product.processor = form.processor.data
        product.screen_size = form.screen_size.data
        product.camera_resolution = form.camera_resolution.data
        product.weight = form.weight.data
        product.display = form.display.data
        product.selfie_camera_resolution = form.selfie_camera_resolution.data
        product.ram = form.ram.data
        product.battery = form.battery.data
        if form.category.data == "Apple":
            product.category = 1
        elif form.category.data == "Samsung":
            product.category = 2
        elif form.category.data == "Xiaomi":
            product.category = 3
        db.session.commit()
        flash('Товар изменен успешно', category="success")
        return redirect(url_for('admin.admin', slug='products'))
    elif request.method == 'GET':
        form.name.data = product.name
        form.price.data = product.price
        form.description.data = product.description
        form.operating_system.data = product.operating_system
        form.storage_capacity.data = product.storage_capacity
        form.processor.data = product.processor
        form.screen_size.data = product.screen_size
        form.camera_resolution.data = product.camera_resolution
        form.weight.data = product.weight
        form.display.data = product.display
        form.selfie_camera_resolution.data = product.selfie_camera_resolution
        form.ram.data = product.ram
        form.battery.data = product.battery
        form.category.data = product.category
    return render_template('admin/edit_product.html', form=form, product_id=product_id)


@admin.route('/add_product', methods=["GET", "POST"])
@login_required
def add_product():
    form = AddProductForm(request.form)
    if request.method == "POST":
        upload_file = request.files.get('product_img')
        file_path = None
        if upload_file:
            file_name = upload_file.filename
            file_path = os.path.join('images/', file_name)
            upload_file.save(os.path.join('phone_store/static', file_path))
        cat_id = 1
        if request.form.get('category') == 'Apple':
            cat_id = 1
        elif request.form.get('category') == 'Samsung':
            cat_id = 2
        elif request.form.get('category') == 'Xiaomi':
            cat_id = 3
        product = Product(
            id=Product.query.order_by(Product.id.desc()).first().id+1,
            name=request.form.get('name'),
            price=request.form.get('price'),
            description=request.form.get('description'),
            product_img=file_path,
            operating_system=request.form.get('operating_system'),
            storage_capacity=request.form.get('storage_capacity'),
            processor=request.form.get('processor'),
            screen_size=request.form.get('screen_size'),
            camera_resolution=request.form.get('camera_resolution'),
            weight=request.form.get('weight'),
            display=request.form.get('display'),
            selfie_camera_resolution=request.form.get('selfie_camera_resolution'),
            ram=request.form.get('ram'),
            battery=request.form.get('battery'),
            category=cat_id,
        )
        db.session.add(product)
        db.session.commit()
        flash("Товар добавлен успешно", category='success')
        return redirect(url_for('admin.admin', slug='products'))
    return render_template('admin/add_product.html', form=form)


@admin.context_processor
def utility_processor():
    def product_name(product_id):
        return Product.query.filter_by(id=product_id).first().name

    def user_name(order_id):
        order = Order.query.filter_by(order_id=order_id).first()
        name = User.query.filter_by(id=order.user_id).first().name
        return name

    return dict(product_name=product_name, user_name=user_name)


@admin.route('/<string:slug>')
@login_required
def admin(slug):
    products = Product.query.all()
    users = User.query.all()
    orders = OrderProduct.query.all()
    products_count = len(products)
    users_count = len(users)
    orders_count = len(orders)
    if slug == "products":
        return render_template('admin/panel.html', products=products, users_count=users_count,
                               products_count=products_count, orders_count=orders_count)
    if slug == "users":
        return render_template('admin/panel.html', users=users, users_count=users_count,
                               products_count=products_count, orders_count=orders_count)
    if slug == "orders":
        return render_template('admin/panel.html', orders=orders, users_count=users_count,
                               products_count=products_count, orders_count=orders_count)
