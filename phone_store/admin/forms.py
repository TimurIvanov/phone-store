from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SelectField, FileField
from wtforms.validators import InputRequired, Length

from phone_store.auth.models import Category


class AddProductForm(FlaskForm):
    name = StringField('Название', validators=[InputRequired(),
                                               Length(min=5, max=30,
                                                      message="Название должно состоять хотя бы из 5 символов")])
    price = IntegerField('Цена', validators=[InputRequired()])
    description = StringField('Описание', validators=[InputRequired()])
    product_img = FileField('Картинка')
    operating_system = StringField('Операционная система', validators=[InputRequired()])
    storage_capacity = IntegerField('Физическая память', validators=[InputRequired()])
    processor = StringField('Процессор', validators=[InputRequired()])
    screen_size = StringField('Диагональ экрана', validators=[InputRequired()])
    camera_resolution = StringField('Основная камера', validators=[InputRequired()])
    weight = IntegerField('Вес', validators=[InputRequired()])
    display = StringField('Тип экрана', validators=[InputRequired()])
    selfie_camera_resolution = StringField('Фронтальная камера', validators=[InputRequired()])
    ram = IntegerField('Оперативная память', validators=[InputRequired()])
    battery = IntegerField('Емкость аккумулятора', validators=[InputRequired()])
    category = SelectField('Категория', choices=[cat.name for cat in Category.query.all()])


class ProductEditForm(FlaskForm):
    name = StringField('Название', validators=[InputRequired(),
                                               Length(min=5, max=30,
                                                      message="Название должно состоять хотя бы из 5 символов")])
    price = IntegerField('Цена', validators=[InputRequired()])
    description = StringField('Описание', validators=[InputRequired()])
    product_img = FileField('Картинка')
    operating_system = StringField('Операционная система', validators=[InputRequired()])
    storage_capacity = IntegerField('Физическая память', validators=[InputRequired()])
    processor = StringField('Процессор', validators=[InputRequired()])
    screen_size = StringField('Диагональ экрана', validators=[InputRequired()])
    camera_resolution = StringField('Основная камера', validators=[InputRequired()])
    weight = IntegerField('Вес', validators=[InputRequired()])
    display = StringField('Тип экрана', validators=[InputRequired()])
    selfie_camera_resolution = StringField('Фронтальная камера', validators=[InputRequired()])
    ram = IntegerField('Оперативная память', validators=[InputRequired()])
    battery = IntegerField('Емкость аккумулятора', validators=[InputRequired()])
    category = SelectField('Категория', choices=[cat.name for cat in Category.query.all()])