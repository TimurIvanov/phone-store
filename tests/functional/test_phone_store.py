import pytest

from werkzeug.security import generate_password_hash

from phone_store import db, app
from phone_store.auth.models import User, Order, OrderProduct, Product


def create_app(config_filename=None):
    app.config.from_pyfile(config_filename)
    return app


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app('../tests/flask_test.cfg')

    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()


@pytest.fixture(scope='module')
def init_database():
    user1 = User(name="Timur", email="timur@mail.ru", password=generate_password_hash("timur123", method='sha256'))
    user2 = User(name="Ivan", email="ivan@mail.ru", password=generate_password_hash("ivan1234", method='sha256'))

    if not (User.query.filter_by(email=user1.email).first() and User.query.filter_by(email=user2.email).first()):
        db.session.add(user1)
        db.session.add(user2)

        db.session.commit()

    yield db


@pytest.fixture(scope='module')
def admin_login_logout(test_client):
    response = test_client.post('/admin/login',
                                data=dict(email='timur@mail.ru',
                                          password='timur123'),
                                follow_redirects=True)

    yield response.request

    test_client.get('/admin/logout', follow_redirects=True)


@pytest.fixture(scope='module')
def user_login_logout(test_client):
    response = test_client.post('/auth/login',
                                data=dict(email='timur@mail.ru',
                                          password='timur123'),
                                follow_redirects=True)

    yield response.request

    test_client.get('/auth/logout', follow_redirects=True)


def test_home_page(test_client):
    response = test_client.get('/')
    assert response.status_code == 200


def test_product_pages(test_client):
    response = test_client.get('/product/1')
    assert response.status_code == 200

    response = test_client.get('/product/2')
    assert response.status_code == 200

    response = test_client.get('/product/3')
    assert response.status_code == 200


def test_valid_login_logout(test_client, init_database):
    # TEST for user Timur
    response = test_client.post('/auth/login',
                                data=dict(email='timur@mail.ru',
                                          password='timur123'),
                                follow_redirects=True)

    assert response.status_code == 200

    response = test_client.get('/auth/logout', follow_redirects=True)
    assert response.status_code == 200

    # TEST for user Ivan
    response = test_client.post('/auth/login',
                                data=dict(email='ivan@mail.ru',
                                          password='ivan1234'),
                                follow_redirects=True)
    assert response.status_code == 200

    response = test_client.get('/auth/logout', follow_redirects=True)
    assert response.status_code == 200


def test_profile_page(test_client, user_login_logout):
    response = test_client.get('/profile')
    assert response.status_code == 200


def test_shopping_cart_page(test_client, user_login_logout):
    response = test_client.get('/shopping-cart')
    assert response.status_code == 200


def test_orders_page(test_client, user_login_logout):
    response = test_client.get('/orders')
    assert response.status_code == 200


def test_user_profile(test_client, init_database, user_login_logout):
    user = User.query.filter_by(email='timur@mail.ru').first()

    assert user.profile.surname == "Ivanov"
    assert user.profile.age == 19
    assert user.profile.delivery_address == "Проспект Победы, 13"
    assert user.profile.mobile_phone == "987654321"


def test_order_product(test_client, init_database, user_login_logout):
    order = Order(user_id=8)
    order_product = OrderProduct(quantity=1, product_id=1)
    order.products.append(order_product)

    if not OrderProduct.query.filter_by(quantity=1, product_id=1).first():
        db.session.add(order)
        db.session.commit()

    user = User.query.filter_by(id=8).first()
    assert user.orders
    assert user.orders[0].products[0].product_id == 1
    assert Product.query.filter_by(id=user.orders[0].products[0].product_id).first().name == "iPhone 11 64Gb"
    assert Product.query.filter_by(id=user.orders[0].products[0].product_id).first().storage_capacity == 64
    assert Product.query.filter_by(id=user.orders[0].products[0].product_id).first().price == 49990
    assert Product.query.filter_by(id=user.orders[0].products[0].product_id).first().operating_system == "IOS 13"


def test_is_admin(test_client):
    admin = User.query.filter_by(email='admin@mail.ru').first()

    assert admin.is_admin is True

    user = User.query.filter_by(email='timur@mail.ru').first()

    assert user.is_admin is False


def test_valid_admin_login_logout(test_client):
    response = test_client.post('/admin/login',
                                data=dict(email='admin@mail.ru',
                                          password='admin123'),
                                follow_redirects=True)

    assert response.status_code == 200

    test_client.get('/admin/logout', follow_redirects=True)


def test_admin_page(test_client, admin_login_logout):
    response = test_client.get('/admin/admin', follow_redirects=True)
    assert response.status_code == 200


def test_product(test_client):
    product = Product.query.get(1)
    assert product.name == 'iPhone 11 64Gb'
    assert product.price == 49990
    assert product.storage_capacity == 64
    assert product.display == 'IPS'
    assert product.weight == 194
